// data
SPECDT = [
  {
    group: "General",
    items: [
      { name: "Model", value: "DC11" },
      { name: "Delivery time", value: "2-3 weeks" },
    ],
  },
  {
    group: "Engine",
    items: [{ name: "bhp", value: "404" }],
  },
  {
    group: "specials",
    text: "convertible, leather seats",
  },
];

const VAT = 0.19;
const PRICE = 19.95;
const DISCOUNT_10 = 0.1;
const DISCOUNT_20 = 0.2;
const DISCOUNT_30 = 0.3;

// vue component
const vueApp = new Vue({
  el: "#spec",
  data: {
    show: 0,
    spec: SPECDT,
    price: PRICE,
    qty: 1,
    msg: [],
  },
  methods: {
    setActiveTab: (i) => {
      localStorage.setItem("activeTab", i);
    },
    validateQty: (value) => {
      if (value === "" || value === 0 || !/^\d+$/.test(value)) {
        return false;
      }
    },
  },
  filters: {
    formatPrice(value) {
      return String(fixRound(value, 2)).replace(".", ",");
    },
  },
  computed: {
    calculatePrice() {
      let total = this.qty * this.price;
      if (this.qty < 1)
        return this.price - (this.price / (1 + VAT)) * DISCOUNT_10;
      if (this.qty >= 3 && this.qty < 5) {
        return total - (total / (1 + VAT)) * DISCOUNT_20;
      } else if (this.qty >= 5) {
        return total - (total / (1 + VAT)) * DISCOUNT_30;
      }
      return total - (total / (1 + VAT)) * DISCOUNT_10;
    },
  },
  watch: {
    // validate entered quantity number
    qty(value) {
      if (this.validateQty(value) === false) {
        this.msg["qty"] = "Please add a valid quantity.";
        // disable calculation if not valid number entered
        if (value > 1) {
          this.price = PRICE;
          this.qty = 1;
        }
      } else {
        this.msg["qty"] = "";
      }
    },
  },
  mounted() {
    var activeTab = localStorage.getItem("activeTab");
    if (activeTab) {
      this.show = activeTab;
    }
  },
});

// fix round
function fixRound(value, decimals) {
  return Number(Math.round(value + "e" + decimals) + "e-" + decimals);
}
