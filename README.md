# README

This is Vue.js tabs with price block for the product page.

### What is inside?

- tabs nicely converts to the accordion on mobile screens (<768px)
- always one of the tab is active
- web browser remembers active tab even on F5 or if user comes back from another site
- interactive price calculation depending on entered quantity (e.g. current price is 19,95€ incl. 19% VAT with -10% discount, from 3 qty -20% discount, from 5 qty -30%)

### Technical notes

- no vue-cli or create-react-app has been used
- no styled components library like Vuetify or similar
- Sass file included (CSS compiled via VS Code plugin)
- product data defined in javascript as a JS object

### How do I get set up?

- clone project to your local directory
- open `info.html` in your web browser
